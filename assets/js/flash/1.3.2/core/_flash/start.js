/**
 * Version: 1.0
 */

/**
 * Execute all the callbacks bound to the DOMContentLoaded event with the flash.ready method.
 * @return {void}
 */
flashCore.prototype.start = function() {
	var self = this;
	// Event to say that Flash frontend core has been initialised
	var flashReadyEvent = new CustomEvent('flashReady');
	document.dispatchEvent(flashReadyEvent);

	// GTM
	self.waitCondition(
		function(){
			return typeof ga !== 'undefined';
		},
		function(){
            if(document.querySelector('meta[name="gtm"]')) {
                var gtm_id = document.querySelector('meta[name="gtm"]').getAttribute('content');
                window.dataLayer = window.dataLayer || [];
                function gtag(){dataLayer.push(arguments);}
                gtag('js', new Date());
                gtag('config', gtm_id);
                ga('send', 'pageview', location.pathname + location.search);
            }
		}
	)

	// Check if the node inserted in each page is there
	// if it's not there it means it's a new page
	// otherwise it's an old page loaded with the back button so the code won't
	// be executed at all
    var flash_executed_js = document.querySelector('.flash_executed_js');

	// Executing the js from the blocks
	(Object.keys(self.startup_functions.named) || []).forEach(function(key){
		if(key.indexOf('.') > -1) {
			var blocks = document.querySelectorAll(key);
		} else {
			var blocks = document.querySelectorAll('.' + key + ',' + key);
		}
		if(flash_executed_js && !self.startup_functions.named[key].always) {
			return;
		}
		(blocks || []).forEach(function(block) {
			// Init the js of a block just when it's entering the viewport
			if(self.startup_functions.named[key].in_viewport) {
				// Init an object with all the callbacks for
				// the js that has to be executed JUST when the block is
				// about to enter the viewport
				if(!window.flash_startup_functions) {
					window.flash_startup_functions = {};
				}

				// Function that check if the block is in the viewport
				// and eventually execute the code
				if(window.flash_startup_functions[key]) {
					window.removeEventListener('scroll', window.flash_startup_functions[key]);
				}
				window.flash_startup_functions[key] = function () {
					// the default offset (200px above the block) can be customised with in_viewport_offset
					if(flash.hasEnteredViewport(block, (self.startup_functions.named[key].in_viewport_offset || 200))) {
						window.removeEventListener('scroll', window.flash_startup_functions[key]);
						self.startup_functions.named[key].callback(block);	
					}
				}

				// Listeners to check when the element will be in the vieport
				window.removeEventListener('scroll', window.flash_startup_functions[key]);
				window.addEventListener('scroll', window.flash_startup_functions[key]);
				window.flash_startup_functions[key]();
			} else {
				// If no settings are set, execute the function straight away
				self.startup_functions.named[key].callback(block);	
			}
		});
	});

	// Executing the js from the blocks without body class manipulation
	(Object.keys(self.startup_functions.anonymous) || []).forEach(function(key){
		if(flash_executed_js && !self.startup_functions.anonymous[key].always) {
			return;
		}
		self.startup_functions.anonymous[key].callback();
	});

	if(!flash_executed_js) {
		// Insert the node used to check if it's an old page on page load
		flash_executed_js = document.createElement('div');
		flash_executed_js.className = 'flash_executed_js';
		document.body.appendChild(flash_executed_js);
	}
}