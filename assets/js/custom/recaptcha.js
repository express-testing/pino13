function recaptchaInit() {
	var $recaptchas = document.querySelectorAll('[data-recaptcha]');
	$recaptchas.forEach(function($el) {
		$el.innerHTML = '';
		var $recaptcha = $el.cloneNode(true);
		$el.insertAdjacentElement('afterend', $recaptcha);
		$el.parentNode.removeChild($el);
		var $form = $recaptcha.closest('form');
		var $submit = $form.querySelector('[type="submit"]');
		if($submit) $submit.disabled = true;
		grecaptcha.render($recaptcha, {
			sitekey: recaptcha_key,
			callback: function() {
				if($submit) $submit.disabled = false;
			}
		});
	});
}

function recaptchaCallback() { recaptchaInit(); }